//
//  ListaTableViewController.m
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import "ListaTableViewController.h"
#import "CustomCell.h"
#import "ShotViewController.h"
#import "GerenciadorService.h"

@interface ListaTableViewController()

@property int page;

@end

@implementation ListaTableViewController

-(void)viewDidLoad {
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.tintColor = [UIColor grayColor];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@""];
    [refresh addTarget:self action:@selector(reload:) forControlEvents:UIControlEventValueChanged];
    
    self.refreshControl = refresh;
    
    self.gerenciador = [GerenciadorService getInstance];
    
    self.gerenciador.listaController = self;
    [self.gerenciador getShots];
}

- (void) reload:(__unused id)sender {
    self.page = 1;
    if(self.gerenciador.lista && self.gerenciador.lista.count > 0) {
        [self.gerenciador.lista removeAllObjects];
    }
    [self.gerenciador getShots];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.gerenciador getShots];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.gerenciador.lista.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ShotModel *shot = [self.gerenciador.lista objectAtIndex:indexPath.row];
    
    ShotViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ShotViewController"];
    controller.shot = shot;
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *nome = @"CustomCell";
    CustomCell *linha = [tableView dequeueReusableCellWithIdentifier:nome];
    
    if(!linha) {
        linha = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nome];
    }
    
    ShotModel *shot = [self.gerenciador.lista objectAtIndex:indexPath.row];
    
    linha.titulo.text = shot.title;
    linha.visualizacoes.text = [NSString stringWithFormat:@"%@ views", [shot.views_count stringValue]];
    
    [linha.imagem setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:shot.image_teaser_url]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        linha.imagem.image = image;
        [linha setNeedsLayout];
        
    } failure:nil];
    
    return linha;
}

@end
