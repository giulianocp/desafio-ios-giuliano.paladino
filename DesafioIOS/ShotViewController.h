//
//  ShotViewController.h
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShotModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ShotViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIImageView *imagem;
@property (nonatomic, strong) IBOutlet UIImageView *imagemPlayer;
@property (nonatomic, strong) IBOutlet UILabel *titulo;
@property (nonatomic, strong) IBOutlet UILabel *nome;
@property (nonatomic, strong) IBOutlet UILabel *visualizacoes;
@property (nonatomic, strong) IBOutlet UITextView *descricao;

@property (nonatomic, strong) ShotModel *shot;

@end
