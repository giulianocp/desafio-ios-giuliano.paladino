//
//  ShotViewController.m
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import "ShotViewController.h"

@implementation ShotViewController

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if(self) {
        //
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.imagem sd_setImageWithURL:[NSURL URLWithString:self.shot.image_url]];
    [self.imagemPlayer sd_setImageWithURL:[NSURL URLWithString:self.shot.player.avatar_url]];
    
    self.titulo.text = self.shot.title;
    self.nome.text = self.shot.player.name_player;
    self.visualizacoes.text = [self.shot.views_count stringValue];
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[self.shot.description_shot dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    self.descricao.attributedText = attrStr;
    
    self.imagemPlayer.layer.cornerRadius = 25;
    self.imagemPlayer.layer.masksToBounds = YES;
    
    self.title = self.shot.title;
}

@end
