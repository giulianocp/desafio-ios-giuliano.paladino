//
//  ListaTableViewController.h
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIRefreshControl+AFNetworking.h"
#import "UIImageView+AFNetworking.h"

@class GerenciadorService;

@interface ListaTableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) GerenciadorService *gerenciador;

@end
