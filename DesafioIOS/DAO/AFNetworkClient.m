//
//  AFNetworkClient.m
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import "AFNetworkClient.h"

static NSString * const baseURLString = @"http://api.dribbble.com/shots/popular?page=1";

@implementation AFNetworkClient

+ (instancetype)sharedClient {
    static AFNetworkClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[AFNetworkClient alloc] initWithBaseURL:[NSURL URLWithString:baseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    return _sharedClient;
}

@end
