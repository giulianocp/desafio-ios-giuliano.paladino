//
//  AFNetworkClient.h
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

@interface AFNetworkClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end
