//
//  GerenciadorService.m
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import "GerenciadorService.h"

@implementation GerenciadorService

static GerenciadorService *instance = nil;

-(instancetype)init {
    self = [super init];
    
    if(self) {
        self.lista = [NSMutableArray new];
        instance = self;
    }
    
    return self;
}

+ (GerenciadorService *) getInstance {
    @synchronized(self)
    {
        if (instance == nil){
            instance = [[self alloc] init];
        }
    }
    return instance;
}

-(void)getShots {
    NSURLSessionTask *task = [API getShots:@"" andBlock:^(NSDictionary *dados, NSError *error) {
        if (!error) {
            self.shots = [MTLJSONAdapter modelOfClass:[ShotsModel class] fromJSONDictionary:dados error:nil];
            if(self.shots) {
                self.lista = [self.shots.shots mutableCopy];
                [self.listaController.tableView reloadData];
            }
        }
    }];
    
    [self.listaController.refreshControl setRefreshingWithStateOfTask:task];
}

@end
