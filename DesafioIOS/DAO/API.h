//
//  API.h
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShotsModel.h"

@interface API : NSObject

+ (NSURLSessionDataTask *)getShots:(NSString *)page andBlock:(void (^)(NSDictionary *dados, NSError *error))block;

@end
