//
//  ShotModel.m
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import "ShotModel.h"

@implementation ShotModel

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    NSDictionary* json = @{
                           @"id_shot" : @"id",
                           @"title" : @"title",
                           @"description_shot" : @"description",
                           @"height" : @"height",
                           @"width" : @"width",
                           @"likes_count" : @"likes_count",
                           @"comments_count" : @"comments_count",
                           @"rebounds_count" : @"rebounds_count",
                           @"url" : @"url",
                           @"short_url" : @"short_url",
                           @"views_count" : @"views_count",
                           @"rebound_source_id" : @"rebound_source_id",
                           @"image_url" : @"image_url",
                           @"image_teaser_url" : @"image_teaser_url",
                           @"player" : @"player",
                           @"created_at" : @"created_at"
                           };
    return json;
}

+(NSValueTransformer *)playerJSONTransformer{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[PlayerModel class]];
}

@end
