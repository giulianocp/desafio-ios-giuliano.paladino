//
//  ShotsModel.m
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import "ShotsModel.h"

@implementation ShotsModel

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    NSDictionary* json = @{
                           @"page" : @"page",
                           @"per_page" : @"per_page",
                           @"pages" : @"pages",
                           @"total" : @"total",
                           @"shots" : @"shots"
                           };
    return json;
}

+(NSValueTransformer *)shotsJSONTransformer{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[ShotModel class]];
}

@end
