//
//  API.m
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import "API.h"
#import "AFNetworkClient.h"

@implementation API

+ (NSURLSessionDataTask *)getShots:(NSString *)page andBlock:(void (^)(NSDictionary *dados, NSError *error))block {
    return [[AFNetworkClient sharedClient] GET:page parameters:nil success:^(NSURLSessionDataTask * __unused task, id JSON) {
        
        if (block) {
            block(JSON, nil);
        }
    } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
}

@end
