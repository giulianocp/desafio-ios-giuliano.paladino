//
//  ShotModel.h
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "PlayerModel.h"

@interface ShotModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSNumber *id_shot;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *description_shot;
@property (nonatomic, copy) NSNumber *height;
@property (nonatomic, copy) NSNumber *width;
@property (nonatomic, copy) NSNumber *likes_count;
@property (nonatomic, copy) NSNumber *comments_count;
@property (nonatomic, copy) NSNumber *rebounds_count;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *short_url;
@property (nonatomic, copy) NSNumber *views_count;
@property (nonatomic, copy) NSNumber *rebound_source_id;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *image_teaser_url;
@property (nonatomic, copy) PlayerModel *player;
@property (nonatomic, copy) NSString *created_at;

@end
