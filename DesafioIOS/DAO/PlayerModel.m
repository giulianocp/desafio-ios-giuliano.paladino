//
//  PlayerModel.m
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import "PlayerModel.h"

@implementation PlayerModel

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    NSDictionary* json = @{
                           @"id_player" : @"id",
                           @"name_player" : @"name",
                           @"location" : @"location",
                           @"followers_count" : @"followers_count",
                           @"draftees_count" : @"draftees_count",
                           @"likes_count" : @"likes_count",
                           @"likes_received_count" : @"likes_received_count",
                           @"comments_count" : @"comments_count",
                           @"comments_received_count" : @"comments_received_count",
                           @"rebounds_count" : @"rebounds_count",
                           @"rebounds_received_count" : @"rebounds_received_count",
                           @"url" : @"url",
                           @"avatar_url" : @"avatar_url",
                           @"username" : @"username",
                           @"twitter_screen_name" : @"twitter_screen_name",
                           @"website_url" : @"website_url",
                           @"drafted_by_player_id" : @"drafted_by_player_id",
                           @"shots_count" : @"shots_count",
                           @"following_count" : @"following_count",
                           @"created_at" : @"created_at"
                           };
    return json;
}

@end
