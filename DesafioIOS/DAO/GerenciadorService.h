//
//  GerenciadorService.h
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "API.h"
#import "ShotsModel.h"
#import "ListaTableViewController.h"

@interface GerenciadorService : NSObject

@property (nonatomic, strong) ShotsModel *shots;
@property (nonatomic, strong) NSMutableArray *lista;
@property (nonatomic, strong) NSMutableArray *listaFiltro;

@property (nonatomic, strong) ListaTableViewController *listaController;

-(void)getShots;

+(GerenciadorService *)getInstance;

@end
