//
//  PlayerModel.h
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface PlayerModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSNumber *id_player;
@property (nonatomic, copy) NSString *name_player;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSNumber *followers_count;
@property (nonatomic, copy) NSNumber *draftees_count;
@property (nonatomic, copy) NSNumber *likes_count;
@property (nonatomic, copy) NSNumber *likes_received_count;
@property (nonatomic, copy) NSNumber *comments_count;
@property (nonatomic, copy) NSNumber *comments_received_count;
@property (nonatomic, copy) NSNumber *rebounds_count;
@property (nonatomic, copy) NSNumber *rebounds_received_count;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *avatar_url;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *twitter_screen_name;
@property (nonatomic, copy) NSString *website_url;
@property (nonatomic, copy) NSNumber *drafted_by_player_id;
@property (nonatomic, copy) NSNumber *shots_count;
@property (nonatomic, copy) NSNumber *following_count;
@property (nonatomic, copy) NSString *created_at;

@end
