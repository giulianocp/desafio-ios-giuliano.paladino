//
//  ShotsModel.h
//  DesafioIOS
//
//  Created Giuliano Paladino on 23/09/15.
//  Copyright (c) 2015 Giuliano Paladino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Mantle/Mantle.h>
#import "ShotModel.h"

@interface ShotsModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *page;
@property (nonatomic, copy) NSNumber *per_page;
@property (nonatomic, copy) NSNumber *pages;
@property (nonatomic, copy) NSNumber *total;
@property (nonatomic, copy) NSArray *shots;

@end
